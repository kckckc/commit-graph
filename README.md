# Commit Graph

Generates a graph of when commits were authored.

Live at [kckckc.gitlab.io/commit-graph](https://kckckc.gitlab.io/commit-graph/).

## Process
- Fetch all projects the user is a member of
- Fetch all commits in the main branch of each repository and record dates when the target user's commit email authored a commit
- Draw graph

Since we only fetch the default branch for performance reasons, commits to other branches aren't counted.

You can add additional commit author emails to filter by in the settings (bottom left).
