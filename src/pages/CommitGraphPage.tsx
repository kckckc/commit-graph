import { Box, Center, Flex, Heading, Link } from '@chakra-ui/react';
import React, { useEffect } from 'react';
import { CommitStats } from '../molecules/CommitStats';
import { AllCommitGraphs } from '../organisms/AllCommitGraphs';
import { CommitGraph } from '../organisms/CommitGraph';
import { commitDataStore, useGetCommitData } from '../util/api';

interface CommitGraphPageProps {}

export const CommitGraphPage: React.FC<CommitGraphPageProps> = ({}) => {
	const getCommitData = useGetCommitData();
	const [{ loading, events, data }] = commitDataStore.hook();

	useEffect(() => {
		getCommitData();
	}, []);

	return (
		<Flex
			flex={1}
			direction="column"
			gap={4}
			h="100%"
			justifyContent="center"
			p={4}
		>
			<Heading size="lg" textAlign="center">
				GitLab Commit Graph
			</Heading>
			{loading ? (
				<>
					<Box textAlign="center">
						Fetching commits for{' '}
						<Link
							isExternal
							href={`https://gitlab.com/${data?.me?.username}`}
							color="blue.400"
						>
							{data?.me?.username ?? '?'}
						</Link>{' '}
						({data?.commits?.length} commits so far)
					</Box>

					<Center>
						<CommitGraph />
					</Center>

					<Box m={4} p={4} borderWidth={1} rounded="lg">
						<Flex
							direction="column-reverse"
							h="50vh"
							overflowY="scroll"
						>
							<Flex direction="column">
								{events.map((e) => (
									<Box key={e}>{e}</Box>
								))}
							</Flex>
						</Flex>
					</Box>
				</>
			) : (
				<>
					<Center>
						<CommitStats />
					</Center>
					<AllCommitGraphs />
				</>
			)}
		</Flex>
	);
};
