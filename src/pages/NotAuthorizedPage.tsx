import { Box, Button, Flex, Heading, Link } from '@chakra-ui/react';
import React from 'react';
import { FiGitlab } from 'react-icons/fi';
import { useSettingsDisclosure } from '../providers/SettingsDisclosure';
import { authorizeGitlab } from '../util/oauth';

interface NotAuthorizedPageProps {}

export const NotAuthorizedPage: React.FC<NotAuthorizedPageProps> = ({}) => {
	const handleAuthorize = () => {
		authorizeGitlab();
	};

	const { onOpen } = useSettingsDisclosure();

	return (
		<Flex
			flex={1}
			direction="column"
			alignItems="center"
			justifyContent="center"
		>
			<Flex direction="column" gap={4} alignItems="center">
				<Heading size="lg" textAlign="center">
					GitLab Commit Graph
				</Heading>

				<Box textAlign="center">
					Generates a graph of when you committed instead of push
					events and other activity.
				</Box>

				<Box textAlign="center">
					Unfortunately only commits to the main branch are counted.
					<br />
					Looping over every branch in large repositories looking for
					only a handful of commits is way too slow.
				</Box>

				<Box textAlign="center">
					You can add additional commit author emails in the{' '}
					<Link
						color="blue.500"
						_dark={{ color: 'blue.400' }}
						onClick={onOpen}
					>
						settings
					</Link>
					.
				</Box>

				<Box>
					<Button leftIcon={<FiGitlab />} onClick={handleAuthorize}>
						Authorize GitLab
					</Button>
				</Box>
			</Flex>
		</Flex>
	);
};
