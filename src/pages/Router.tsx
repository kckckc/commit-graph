import { Box, Flex } from '@chakra-ui/react';
import React from 'react';
import { Footer } from '../organisms/Footer';
import { SettingsModal } from '../organisms/SettingsModal';
import { useAccessToken } from '../providers/AccessToken';
import { CommitGraphPage } from './CommitGraphPage';
import { NotAuthorizedPage } from './NotAuthorizedPage';

interface RouterProps {}

export const Router: React.FC<RouterProps> = ({}) => {
	const accessToken = useAccessToken();

	return (
		<Flex direction="column" minH="100%">
			{accessToken === '' ? <NotAuthorizedPage /> : <CommitGraphPage />}

			<Footer />

			<SettingsModal />
		</Flex>
	);
};
