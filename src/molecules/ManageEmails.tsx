import {
	Box,
	Button,
	Code,
	Flex,
	Heading,
	IconButton,
	Input,
} from '@chakra-ui/react';
import React, { useState } from 'react';
import { FiPlus, FiTrash } from 'react-icons/fi';
import useLocalStorage from '../util/useLocalStorage';

interface ManageEmailsProps {}

export const ManageEmails: React.FC<ManageEmailsProps> = ({}) => {
	const [emailMap] = useLocalStorage<Record<string, number>>(
		'suggested_email_map',
		{}
	);

	const [emailListRaw, setEmailListRaw] = useLocalStorage(
		'commit_author_emails',
		''
	);
	const emailList = emailListRaw.split(',').filter((x) => !!x.length);
	const setEmailList = (list: string[]) => {
		setEmailListRaw(list.join(','));
	};

	const [editEmail, setEditEmail] = useState('');

	const addCurrentEditEmail = () => {
		const newList = emailList.slice(0);
		if (!newList.includes(editEmail.trim())) {
			newList.push(editEmail.trim());
		}
		setEmailList(newList);
		setEditEmail('');

		return false;
	};

	const isEmailValid = !!editEmail.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);

	return (
		<Flex direction="column" gap={2}>
			<Heading size="sm">Additional Emails</Heading>

			<Box>
				Each commit's author is checked against this list of emails. You
				will need to refetch commits for changes to take effect. Added
				emails will be persisted.
			</Box>

			<Flex direction="row" gap={2}>
				<Input
					flex={1}
					type="email"
					value={editEmail}
					onChange={(e) => setEditEmail(e.target.value)}
					onKeyDown={(e) => {
						if (e.key === 'Enter' && isEmailValid) {
							addCurrentEditEmail();
						}
					}}
				></Input>

				<Button
					type="submit"
					colorScheme="blue"
					disabled={!isEmailValid}
					onClick={addCurrentEditEmail}
				>
					Add
				</Button>
			</Flex>

			<Flex direction="column" gap={2}>
				{emailList.map((email) => {
					return (
						<Flex
							key={email}
							direction="row"
							alignItems="center"
							gap={2}
						>
							<IconButton
								aria-label={'Remove email'}
								icon={<FiTrash />}
								size="sm"
								colorScheme="red"
								onClick={() => {
									const newList = emailList
										.slice(0)
										.filter((e) => e !== email);
									setEmailList(newList);
								}}
							/>
							<Code>{email}</Code>
						</Flex>
					);
				})}
			</Flex>

			<Heading size="sm">Suggested Emails</Heading>
			<Flex direction="column" gap={2}>
				{Object.entries(emailMap)
					.sort((a, b) => b[1] - a[1])
					.map(([email, count]) => {
						if (emailList.includes(email)) return null;

						return (
							<Flex
								key={email}
								direction="row"
								alignItems="center"
								gap={2}
							>
								<IconButton
									aria-label={'Add email'}
									icon={<FiPlus />}
									size="sm"
									colorScheme="green"
									onClick={() => {
										const newList = emailList.slice(0);
										newList.push(email);
										setEmailList(newList);
									}}
								/>
								<Code>{email}</Code>
								<Box>({count} commits)</Box>
							</Flex>
						);
					})}
			</Flex>
		</Flex>
	);
};
