import { Box } from '@chakra-ui/react';
import { differenceInDays, format } from 'date-fns';
import React from 'react';
import { commitDataStore } from '../util/api';

interface CommitStatsProps {}

export const CommitStats: React.FC<CommitStatsProps> = ({}) => {
	const [
		{
			data: { commits },
		},
	] = commitDataStore.hook();

	if (!commits.length) return null;

	const earliestTimestamp = commits
		.map((d) => d.getTime())
		.sort((a, b) => a - b)[0];
	const earliestDate = new Date(earliestTimestamp);

	const numDays = differenceInDays(new Date(), earliestDate);

	return (
		<Box>
			{commits.length} commits since {format(earliestDate, 'PPP')}{' '}
			(average {(commits.length / numDays).toFixed(2)} commits per day)
		</Box>
	);
};
