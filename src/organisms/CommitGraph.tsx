import { Box, Flex, Heading } from '@chakra-ui/react';
import { add, format, isSameDay, previousMonday, sub } from 'date-fns';
import React, { ReactNode, useEffect, useMemo, useState } from 'react';
import { Tile } from '../atoms/Tile';
import { commitDataStore } from '../util/api';
import { useThrottleCallback } from '@react-hook/throttle';

interface CommitGraphProps {
	backYears?: number;
}

export const CommitGraph: React.FC<CommitGraphProps> = ({ backYears = 0 }) => {
	const [
		{
			data: { commits },
		},
	] = commitDataStore.hook();

	if (!commits) return null;

	const [rerender, setRerender] = useState(0);

	const throttledRerender = useThrottleCallback(() => {
		setRerender((x) => x + 1);
	}, 1);

	useEffect(() => {
		throttledRerender();
	}, [commits]);

	const [targetYear, body] = useMemo(() => {
		const cols: ReactNode[] = [];

		const targetYear = new Date().getFullYear() - backYears;
		const startDate = new Date(targetYear, 0, 1);

		const makeTile = (weekStart: Date, weekDay: number) => {
			const targetDay = add(weekStart, { days: weekDay });

			const tooltip = format(targetDay, 'MMMM do');

			if (targetDay.getFullYear() !== targetYear)
				return <Tile tooltip={tooltip} empty />;
			if (targetDay.getTime() > Date.now())
				return <Tile tooltip={tooltip} empty />;

			const num = commits.filter((d) => isSameDay(d, targetDay)).length;
			return <Tile tooltip={tooltip}>{num < 1 ? null : num}</Tile>;
		};

		for (let i = 0; i < 54; i++) {
			const weekStart = previousMonday(add(startDate, { days: 7 * i }));

			cols.push(
				<Flex key={i} direction="column">
					{makeTile(weekStart, 0)}
					{makeTile(weekStart, 1)}
					{makeTile(weekStart, 2)}
					{makeTile(weekStart, 3)}
					{makeTile(weekStart, 4)}
					{makeTile(weekStart, 5)}
					{makeTile(weekStart, 6)}
				</Flex>
			);
		}

		return [targetYear, cols];
	}, [rerender]);

	return (
		<Box>
			<Heading size="sm" mb={2}>
				{targetYear}
			</Heading>
			<Flex direction="row" alignItems="center">
				{/* Header */}
				<Flex direction="column">
					<Tile empty>M</Tile>
					<Tile empty></Tile>
					<Tile empty>W</Tile>
					<Tile empty></Tile>
					<Tile empty>F</Tile>
					<Tile empty></Tile>
					<Tile empty></Tile>
				</Flex>
				{body}
			</Flex>
		</Box>
	);
};
