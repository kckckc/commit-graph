import { Flex } from '@chakra-ui/react';
import React, { ReactNode } from 'react';
import { commitDataStore } from '../util/api';
import { CommitGraph } from './CommitGraph';

interface AllCommitGraphsProps {}

export const AllCommitGraphs: React.FC<AllCommitGraphsProps> = ({}) => {
	const [{ loading, data }] = commitDataStore.hook();

	if (loading) return null;

	const earliestTimestamp = data.commits
		.map((d) => d.getTime())
		.sort((a, b) => a - b)[0];
	const earliestYear = new Date(earliestTimestamp).getFullYear();
	const currentYear = new Date().getFullYear();

	const graphs: ReactNode[] = [];
	for (let i = earliestYear; i <= currentYear; i++) {
		graphs.push(<CommitGraph key={i} backYears={currentYear - i} />);
	}

	return (
		<Flex direction="column" gap={4} alignItems="center">
			{graphs}
		</Flex>
	);
};
