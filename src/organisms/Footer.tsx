import { Box, Flex, Link } from '@chakra-ui/react';
import React from 'react';
import { ColorModeButton } from '../atoms/ColorModeButton';
import { SettingsButton } from '../atoms/SettingsButton';
import { SourceCodeButton } from '../atoms/SourceCodeButton';

interface FooterProps {}

export const Footer: React.FC<FooterProps> = ({}) => {
	return (
		<Flex direction="row" gap={2} m={4} alignItems="center">
			<SettingsButton />
			<ColorModeButton />
			<SourceCodeButton />
			<Box ml={4}>
				by{' '}
				<Link
					isExternal
					href="https://kckckc.dev"
					color="blue.500"
					_dark={{ color: 'blue.400' }}
				>
					kckckc
				</Link>
			</Box>
		</Flex>
	);
};
