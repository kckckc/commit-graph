import {
	Box,
	Button,
	Flex,
	Heading,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	Switch,
	useColorMode,
} from '@chakra-ui/react';
import React from 'react';
import { ManageEmails } from '../molecules/ManageEmails';
import { useSettingsDisclosure } from '../providers/SettingsDisclosure';

interface SettingsModalProps {}

export const SettingsModal: React.FC<SettingsModalProps> = ({}) => {
	const { isOpen, onClose } = useSettingsDisclosure();

	const { colorMode, toggleColorMode } = useColorMode();

	return (
		<Modal isOpen={isOpen} onClose={onClose} size="lg">
			<ModalOverlay />
			<ModalContent>
				<ModalHeader>Settings</ModalHeader>
				<ModalCloseButton />
				<ModalBody>
					<Box>
						<Heading size="sm" mb={2}>
							Dark Mode
						</Heading>
						<Switch
							isChecked={colorMode === 'dark'}
							onChange={toggleColorMode}
						/>
					</Box>

					<Box mt={4}>
						<ManageEmails />
					</Box>
				</ModalBody>

				<ModalFooter>
					<Button onClick={onClose}>Close</Button>
				</ModalFooter>
			</ModalContent>
		</Modal>
	);
};
