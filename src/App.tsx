import React from 'react';
import { Router } from './pages/Router';
import { AccessTokenProvider } from './providers/AccessToken';
import { SettingsDisclosureProvider } from './providers/SettingsDisclosure';
import { ThemeProvider } from './providers/Theme';

interface AppProps {}

export const App: React.FC<AppProps> = ({}) => {
	return (
		<ThemeProvider>
			<AccessTokenProvider>
				<SettingsDisclosureProvider>
					<Router />
				</SettingsDisclosureProvider>
			</AccessTokenProvider>
		</ThemeProvider>
	);
};

export default App;
