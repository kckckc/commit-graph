import { ChakraProvider, extendTheme, ThemeConfig } from '@chakra-ui/react';
import React, { ReactNode } from 'react';

interface ThemeProviderProps {
	children?: ReactNode;
}

const config: ThemeConfig = {
	initialColorMode: 'system',
	useSystemColorMode: false,
};

const theme = extendTheme({ config });

export const ThemeProvider: React.FC<ThemeProviderProps> = ({ children }) => {
	return <ChakraProvider theme={theme}>{children}</ChakraProvider>;
};
