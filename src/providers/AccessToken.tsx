import React, {
	createContext,
	ReactNode,
	useContext,
	useEffect,
	useState,
} from 'react';
import { getAccessToken } from '../util/oauth';

const AccessToken = createContext('');

interface AccessTokenProviderProps {
	children?: ReactNode;
}

export const AccessTokenProvider: React.FC<AccessTokenProviderProps> = ({
	children,
}) => {
	const [accessToken, setAccessToken] = useState('');

	// On first load, check for auth code
	useEffect(() => {
		const currentUrl = new URL(window.location.href);
		const codeParam = currentUrl.searchParams.get('code');

		if (codeParam) {
			// Remove from url
			currentUrl.searchParams.delete('code');
			history.replaceState({}, '', currentUrl.toString());

			// Get access token using code
			getAccessToken(codeParam).then((token) => {
				setAccessToken(token);
			});
		}
	}, []);

	return (
		<AccessToken.Provider value={accessToken}>
			{children}
		</AccessToken.Provider>
	);
};

export const useAccessToken = () => {
	const accessToken = useContext(AccessToken);
	return accessToken;
};
