import { useDisclosure } from '@chakra-ui/react';
import React, { createContext, ReactNode, useContext } from 'react';

export const SettingsDisclosure = createContext<
	ReturnType<typeof useDisclosure>
>({
	isOpen: false,
	onOpen: () => {},
	onClose: () => {},
	onToggle: () => {},
	isControlled: false,
	getButtonProps: (props?: any) => {},
	getDisclosureProps: (props?: any) => {},
});

interface SettingsDisclosureProviderProps {
	children?: ReactNode;
}

export const SettingsDisclosureProvider: React.FC<
	SettingsDisclosureProviderProps
> = ({ children }) => {
	const disclosure = useDisclosure();

	return (
		<SettingsDisclosure.Provider value={disclosure}>
			{children}
		</SettingsDisclosure.Provider>
	);
};

export const useSettingsDisclosure = () => {
	return useContext(SettingsDisclosure);
};
