import { useCallback } from 'react';
import { useAccessToken } from '../providers/AccessToken';
import { Observable } from './observable';
import throttle from 'lodash.throttle';

export const commitDataStore = new Observable<{
	loading: boolean;
	events: string[];
	data: {
		me: any;
		commits: Date[];
	};
}>({
	loading: false,
	events: [],
	data: {
		me: {},
		commits: [],
	},
});

const loadingLock = new Observable(false);

export const useGetCommitData = () => {
	const accessToken = useAccessToken();

	const callback = useCallback(() => {
		if (accessToken === '') return;

		if (loadingLock.get()) return;

		const task = async () => {
			loadingLock.set(true);

			await getCommitData(accessToken);

			loadingLock.set(false);
		};
		task();
	}, [accessToken]);

	return callback;
};

const getCommitData = async (accessToken: string) => {
	const baseUrl = 'https://gitlab.com/api/v4';

	const doRequest = (path: string, params?: Record<string, string>) => {
		const queryString = new URLSearchParams(params ?? {}).toString();
		const url =
			baseUrl + path + (queryString.length ? '?' + queryString : '');

		return fetch(url, {
			method: 'GET',
			headers: {
				Authorization: 'Bearer ' + accessToken,
			},
		}).then((res) => res.json());
	};

	commitDataStore.set({
		events: [],
		loading: true,
		data: {
			me: null,
			commits: [],
		},
	});

	const myCommitsHashmap: Record<string, boolean> = {};
	const myCommitsList: any[] = [];
	let me: any = null;

	const pushProgressEvent = (desc: string) => {
		const d = commitDataStore.get();
		d.events.push(desc);
		d.loading = true;
		d.data = { me, commits: myCommitsList };
		commitDataStore.set(d);
	};

	pushProgressEvent('starting...');

	pushProgressEvent('fetching user details');
	me = await doRequest('/user');
	console.log(me);

	const myEmails = JSON.parse(
		localStorage.getItem('commit_author_emails') ?? '""'
	)
		.split(',')
		.filter((x: any) => !!x.length);
	if (!myEmails.includes(me.commit_email)) {
		myEmails.push(me.commit_email);
	}

	const promiseQueue: Promise<any>[] = [];
	const enqueue = (prom: Promise<any>) => {
		promiseQueue.push(prom);

		// Remove from queue
		prom.then(() => {
			const idx = promiseQueue.findIndex((q) => q === prom);
			if (idx > -1) {
				promiseQueue.splice(idx, 1);
			}
		});
	};
	const queueIsEmpty = () => {
		return new Promise((resolve, reject) => {
			const intervalId = setInterval(() => {
				if (promiseQueue.length === 0) {
					resolve(true);
					clearInterval(intervalId);
				}
			}, 50);
		});
	};

	// Get all projects user is a member of
	const getAllProjectIds = async () => {
		const projectIds: number[] = [];

		let currentPage = 0;
		const fetchNextPage = async () => {
			currentPage++;
			pushProgressEvent(`fetching user projects (page ${currentPage})`);
			const page = await doRequest(`/projects`, {
				membership: 'true',
				simple: 'true',
				per_page: '100',
				page: currentPage.toString(),
			});

			page.forEach((project: any) => {
				projectIds.push(project.id);
			});

			if (page.length > 0) {
				enqueue(fetchNextPage());
			}
		};

		enqueue(fetchNextPage());
		enqueue(fetchNextPage());
		enqueue(fetchNextPage());
		enqueue(fetchNextPage());

		await queueIsEmpty();

		return projectIds;
	};

	// const getAllBranches = async (projectIds: number[]) => {
	// 	const branches: any[] = [];

	// 	let currentId = -1;
	// 	const fetchNextPage = async () => {
	// 		currentId++;
	// 		const pid = projectIds[currentId];
	// 		// console.log(currentId, pid);
	// 		const page = await doRequest(
	// 			`/projects/${pid}/repository/branches`
	// 		);

	// 		page.forEach((branch: any) => {
	// 			branches.push({
	// 				project: pid,
	// 				branch: branch.name,
	// 			});
	// 		});

	// 		if (currentId < projectIds.length - 1) {
	// 			enqueue(fetchNextPage());
	// 		}
	// 	};

	// 	enqueue(fetchNextPage());
	// 	enqueue(fetchNextPage());
	// 	enqueue(fetchNextPage());
	// 	enqueue(fetchNextPage());

	// 	await queueIsEmpty();

	// 	return branches;
	// };

	const emailMap: Record<string, number> = {};
	const updateEmailMapThrottled = throttle(() => {
		localStorage.setItem('suggested_email_map', JSON.stringify(emailMap));
		// Notify UI
		window.dispatchEvent(new Event('local-storage'));
	}, 500);

	const commitPageQueue: { projectId: number; page: number }[] = [];

	const getCommitsPageForBranch = async (
		projectId: number,
		pageNumber: number
	) => {
		pushProgressEvent(
			`fetching commits for ${projectId} page ${pageNumber} (${commitPageQueue.length} queued)`
		);
		const pageData = await doRequest(
			`/projects/${projectId}/repository/commits`,
			{
				per_page: '100',
				page: pageNumber.toString(),
				// ref_name: ref,
			}
		);

		// Probably errored, project missing repository or unavailable?
		if (!Array.isArray(pageData)) {
			return;
		}

		try {
			pageData.forEach((commit: any) => {
				// Count author emails
				emailMap[commit.author_email] =
					(emailMap[commit.author_email] ?? 0) + 1;

				// Save date if matches
				if (myEmails.includes(commit.author_email)) {
					if (!myCommitsHashmap[commit.id]) {
						myCommitsHashmap[commit.id] = true;
						const date = new Date(commit.created_at);
						myCommitsList.push(date);
					}
				}
			});
		} catch (e) {
			console.log(pageData);
		}

		// Save emailMap periodically
		updateEmailMapThrottled();

		// Queue next page if this page was not empty
		if (pageData.length > 0) {
			commitPageQueue.push({ projectId, page: pageNumber + 1 });
		}
	};

	const projectIds = await getAllProjectIds();

	// This is way too slow
	// We are getting the same commits over and over beacuse branches can include the same commits
	// const branches = await getAllBranches(projectIds);
	// console.log(`num branches: ${branches.length}`);
	// for (let i = 0; i < branches.length; i++) {
	// 	await getAllCommitsForBranch(branches[i].project, branches[i].branch);
	// }

	// Worker
	const fetchCommitsUntilExhausted = async () => {
		while (true) {
			const job = commitPageQueue.shift();
			if (!job) {
				break;
			}

			await getCommitsPageForBranch(job.projectId, job.page);
		}
	};

	// Populate queue
	for (let i = 0; i < projectIds.length; i++) {
		commitPageQueue.push({
			projectId: projectIds[i],
			page: 1,
		});
	}

	const spawnWorkers = 4;
	await Promise.all(
		new Array(spawnWorkers).fill(0).map(() => fetchCommitsUntilExhausted())
	);

	// Final
	const d = commitDataStore.get();
	d.loading = false;
	d.data = { me, commits: myCommitsList };
	commitDataStore.set(d);
};
