const baseUrl = 'https://gitlab.com';

// PKCE https://github.com/curityio/pkce-javascript-example
const generateCodeVerifier = (length: number) => {
	let text = '';
	let possible =
		'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

	for (let i = 0; i < length; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}

	return text;
};
const generateCodeChallenge = async (codeVerifier: string) => {
	let digest = await crypto.subtle.digest(
		'SHA-256',
		new TextEncoder().encode(codeVerifier)
	);

	return btoa(String.fromCharCode(...new Uint8Array(digest)))
		.replace(/=/g, '')
		.replace(/\+/g, '-')
		.replace(/\//g, '_');
};

// https://gitlab.example.com/oauth/authorize?client_id=APP_ID&redirect_uri=REDIRECT_URI&response_type=code&state=STATE&scope=REQUESTED_SCOPES&code_challenge=CODE_CHALLENGE&code_challenge_method=S256
const getAuthRequestUrl = async () => {
	const params: Record<string, string> = {
		client_id:
			'b9690cdd400ca4098d08c7f22ff2c1d5565331be061e07f7d85e6f481b868970',
		scope: 'read_api',
		response_type: 'code',
		// state: 'a',
		code_challenge_method: 'S256',
	};

	// Read redirect uri dynamically
	params.redirect_uri = window.location.href.split('?')[0];

	// PKCE
	const codeVerifier = generateCodeVerifier(64);
	const codeChallenge = await generateCodeChallenge(codeVerifier);
	params.code_challenge = codeChallenge;

	// Store for later
	window.sessionStorage.setItem('code_verifier', codeVerifier);

	const queryString = new URLSearchParams(params).toString();
	const requestUrl = baseUrl + '/oauth/authorize?' + queryString;

	return requestUrl;
};

export const authorizeGitlab = async () => {
	const url = await getAuthRequestUrl();
	window.location.href = url;
};

export const getAccessToken = async (code: string) => {
	const codeVerifier = sessionStorage.getItem('code_verifier');
	if (!codeVerifier) {
		throw new Error('missing code verifier, unable to get access token');
	}

	sessionStorage.removeItem('code_verifier');

	const params: Record<string, string> = {
		client_id:
			'b9690cdd400ca4098d08c7f22ff2c1d5565331be061e07f7d85e6f481b868970',
		code: code,
		code_verifier: codeVerifier,
		grant_type: 'authorization_code',
	};

	// Read redirect uri dynamically
	params.redirect_uri = window.location.href.split('?')[0];

	const queryString = new URLSearchParams(params).toString();
	const requestUrl = baseUrl + '/oauth/token?' + queryString;

	const response = await fetch(requestUrl, {
		method: 'POST',
		mode: 'cors',
	}).then((res) => res.json());

	return response.access_token;
};
