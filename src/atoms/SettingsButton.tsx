import { IconButton, Tooltip } from '@chakra-ui/react';
import React from 'react';
import { FiSettings } from 'react-icons/fi';
import { useSettingsDisclosure } from '../providers/SettingsDisclosure';

interface SettingsButtonProps {}

export const SettingsButton: React.FC<SettingsButtonProps> = ({}) => {
	const { onOpen } = useSettingsDisclosure();

	return (
		<Tooltip label={`Settings`}>
			<IconButton
				aria-label={'Open settings'}
				onClick={onOpen}
				icon={<FiSettings />}
			/>
		</Tooltip>
	);
};
