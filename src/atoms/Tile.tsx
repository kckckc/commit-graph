import { Box, Tooltip } from '@chakra-ui/react';
import React, { ReactNode } from 'react';

interface TileProps {
	children?: ReactNode;
	empty?: boolean;
	tooltip?: string;
}

export const Tile: React.FC<TileProps> = ({ children, empty, tooltip }) => {
	const tile = (
		<Box
			w="16px"
			h="16px"
			borderWidth={empty ? 'none' : '1px'}
			textAlign="center"
			fontSize="2xs"
		>
			{children}
		</Box>
	);

	if (tooltip) {
		return <Tooltip label={tooltip}>{tile}</Tooltip>;
	} else {
		return tile;
	}
};
