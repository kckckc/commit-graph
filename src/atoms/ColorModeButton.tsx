import { IconButton, Tooltip, useColorMode } from '@chakra-ui/react';
import React from 'react';
import { FiMoon, FiSun } from 'react-icons/fi';

interface ColorModeButtonProps {}

export const ColorModeButton: React.FC<ColorModeButtonProps> = ({}) => {
	const { colorMode, toggleColorMode } = useColorMode();

	return (
		<Tooltip
			label={`Switch to ${colorMode === 'light' ? 'dark' : 'light'} mode`}
		>
			<IconButton
				aria-label={'Toggle dark mode'}
				onClick={toggleColorMode}
				icon={colorMode === 'light' ? <FiSun /> : <FiMoon />}
			/>
		</Tooltip>
	);
};
