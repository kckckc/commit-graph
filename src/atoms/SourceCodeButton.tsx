import { IconButton, Link, Tooltip } from '@chakra-ui/react';
import React from 'react';
import { FiGitlab } from 'react-icons/fi';

const SOURCE_CODE_URL = 'https://gitlab.com/kckckc/commit-graph';

interface SourceCodeButtonProps {}

export const SourceCodeButton: React.FC<SourceCodeButtonProps> = ({}) => {
	return (
		<Tooltip label="View source code">
			<Link isExternal href={SOURCE_CODE_URL}>
				<IconButton
					aria-label={'Link to source code'}
					icon={<FiGitlab />}
				/>
			</Link>
		</Tooltip>
	);
};
